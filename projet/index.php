<?php

require 'vendor/autoload.php';
use \giftbox\controler\ControlerCatalogue;
use \giftbox\controler\ControlerCoffret;
use \giftbox\controler\ControlerCagnotte;
use \giftbox\controler\ControlerUrlCadeau;
use \giftbox\controler\ControlerConnexion;
use \giftbox\model\Prestation;
use \giftbox\model\Categorie;

echo '<meta charset="UTF-8">';

$app = new \Slim\Slim();

$app->get('/css',function(){})->name('css');

$app->get('/', function(){
	(new ControlerCatalogue())->index();
})->name('racine');

$app->get('/prestations/', function(){
	(new ControlerCatalogue())->prestations();
})->name('prestations');

$app->get('/prestation/:id', function($id){
	(new ControlerCatalogue())->prestation($id);
})->name('prestation');

$app->get('/prestations/prix', function(){
	(new ControlerCatalogue())->prestationsPrix();
})->name('prix');

$app->get('/prestations/prixdesc', function(){
	(new ControlerCatalogue())->prestationsPrixDesc();
})->name('prixdesc');

$app->get('/categorie/:id', function($id){
	(new ControlerCatalogue())->prestationsByCatg($id);
})->name('categorie');

$app->get('/categorie/:id/prix', function($id){
	(new ControlerCatalogue())->prestationsByCatgPrix($id);
})->name('categorieprix');

$app->get('/categorie/:id/prixdesc', function($id){
	(new ControlerCatalogue())->prestationsByCatgPrixDesc($id);
})->name('categorieprixdesc');

$app->get('/categories/', function(){
	(new ControlerCatalogue())->categories();
})->name('categories');

$app->get('/coffret/', function(){
	(new ControlerCoffret())->coffret();
})->name('coffret');

$app->get('/coffret/valider', function(){
	(new ControlerCoffret())->valider();
})->name('valider');

$app->post('/coffret/valider', function(){
	(new ControlerCoffret())->valider();
})->name('validerP');

$app->get('/coffret/:id/paiement', function($id){
	(new ControlerCoffret())->paiement($id);
})->name('paiement');

$app->get('/coffret/:id/password', function($id){
	(new ControlerCoffret())->modifyPassword($id);
});

$app->get('/coffret/supprimer/:id', function($id){
	(new ControlerCatalogue())->supprimerPrestation($id);
})->name('supprPrestation');

$app->get('/coffret/reactiver/:id', function($id){
	(new ControlerCatalogue())->reactiverPrestation($id);
})->name('reactiverPrestation');

$app->get('/coffret/desactiver/:id', function($id){
	(new ControlerCatalogue())->desactiverPrestation($id);
})->name('desactiverPrestation');

$app->get('/coffret/del/:id', function($id){
	(new ControlerCoffret())->delPrestation($id);
})->name('supprimerPrestation');

$app->get('/coffret/add/:id', function($id){
	(new ControlerCoffret())->addPrestation($id);
})->name('ajouterPrestation');

$app->get('/coffret/:id', function($id){
	(new ControlerCoffret())->coffret($id);
})->name('coffretBD');

$app->post('/coffret/:id', function($id){
	(new ControlerCoffret())->coffret($id);
})->name('coffretBDPost');

$app->get('/addnote/:id/:note', function($id,$note){
	(new ControlerCatalogue())->addNote($id,$note);
})->name('newNote');

$app->post('/coffret/:id/urlcadeau', function($id){
	(new ControlerUrlCadeau())->genererUrlCadeau($id);
})->name('urlCadeau');

$app->get('/cadeau/:token', function($token){
	(new ControlerUrlCadeau())->urlCadeauToken($token);
})->name('cadeau');

$app->get('/cagnotte/participation/:token', function($token){
	(new ControlerCagnotte())->participation($token);
})->name('participation');

$app->get('/cagnotte/gestion/:token', function($token){
	(new ControlerCagnotte())->gestion($token);
})->name('gestion');

$app->post('/cagnotte/addmontant/:token', function($token){
	(new ControlerCagnotte())->addMontant($token);
})->name('newMontant');

$app->post('/cagnotte/cloture/:token',function($token){
	(new ControlerCagnotte())->cloture($token);
})->name('cloture');

$app->post('/',function(){
	(new ControlerConnexion())->login();
})->name('login');

$app->post('/logout',function(){
	(new ControlerConnexion())->logout();
})->name('logout');

$app->run();
