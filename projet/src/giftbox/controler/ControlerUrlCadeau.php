<?php

namespace giftbox\controler;

use \giftbox\model\DBConnection;
use \giftbox\model\Coffret;
use \giftbox\model\Appartient;
use \giftbox\model\Prestation;
use giftbox\view\VueUrlCadeau;

class ControlerUrlCadeau {

		public function genererUrlCadeau($id){
			DBConnection::getInstance();
			$presta = Coffret::where('id','=',$id)->first()->toArray();
			$vue=new VueUrlCadeau();
			//La suite est à faire dans la vue
			if($presta['token'] != ''){
				$vue->render(VueUrlCadeau::URL_CADEAU_DEJA_ENVOYE,null,$token);
			}
			else{
				$token = bin2hex(openssl_random_pseudo_bytes(16));
				//Coffret::find($id)->update(['token' => $token]);
				Coffret::where('id','=', $id)->update(['token' => $token]);
				//ENVOYER PAR EMAIL
				$vue->render(VueUrlCadeau::URL_CADEAU_ENVOI, $id, $token, $_POST['email']);
			}
		}

		public function urlCadeauToken($token){
			DBConnection::getInstance();
			$coffret = Coffret::where('token','=', $token)->first();
			if($coffret == null){
				$vue = new VueUrlCadeau();
				$vue->render(VueUrlCadeau::URL_CADEAU_ERREUR);
			}else{
				$coffretArray = $coffret->toArray();
				$idCoffret = $coffretArray['id'];
				$listeAppartient = Appartient::where('cof_id','=', $idCoffret)->get()->toArray();
				$listePresta;
				foreach($listeAppartient as $value){
					$listePresta[] = Prestation::where('id','=', $value['pre_id'])->first()->toArray();
				}
				$coffret->opened=true;
				$coffret->save();
				$vue = new VueUrlCadeau();
				$vue->render(VueUrlCadeau::URL_CADEAU_AFFICHER, $idCoffret, null, null, $listePresta);
			}
		}
}
