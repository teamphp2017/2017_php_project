<?php

namespace giftbox\controler;

use \giftbox\model\DBConnection;
use \giftbox\model\Utilisateur;

class ControlerConnexion {

  public function login(){
    session_start();
    DBConnection::getInstance();
    $app=\Slim\Slim::getInstance();
    if((isset($_POST['login']) && isset($_POST['password']))){
      $account=Utilisateur::where('login','=',$_POST['login'])->first();
      if(!is_null($account) && password_verify($_POST['password'], $account->password)){
        $_SESSION['isConnected']=true;
        $_SESSION['login']=$_POST['login'];
        $_SESSION['password']=$_POST['password'];
      }
    }
    header ('Location: '.$app->urlFor('racine') );
    exit;
  }

  public static function verify(){
    DBConnection::getInstance();
    $account=Utilisateur::where('login','=',$_SESSION['login'])->first();
      if(!is_null($account) && password_verify($_SESSION['password'],$account->password)){
        return true;
      }
      else{
        unset($_SESSION['login']);
        unset($_SESSION['password']);
        unset($_SESSION['isConnected']);
      }
    return false;
  }

  public function logout(){
    DBConnection::getInstance();
    session_start();
    $app=\Slim\Slim::getInstance();
    unset($_SESSION['login']);
    unset($_SESSION['password']);
    unset($_SESSION['isConnected']);
    header ('Location: '.$app->urlFor('racine') );
    exit;
  }

}
