<?php

namespace giftbox\controler;

use \giftbox\model\Cagnotte;
use \giftbox\model\Participation;
use \giftbox\model\Coffret;
use \giftbox\model\Appartient;
use \giftbox\model\Prestation;
use \giftbox\model\DBConnection;
use \giftbox\view\VueCagnotte;

class ControlerCagnotte{

	public function addMontant($token){
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$participation=new Participation();
		$cagnotte = Cagnotte::where('token_participation','=',$token)->first()->toArray();
		if(isset($_POST['montant']) && !is_null($cagnotte) && $_POST['montant']!=0){
			$participation->montant=$_POST['montant'];
			$participation->cagn_id=$cagnotte['id'];
			$participation->save();
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}

	public function linkGestion($token){
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$cagnotte = Cagnotte::where('token_gestion','=',$token)->first();
		header ("Location: ".$app->urlFor('gestion',array('token'=>$cagnotte->token_gestion)) );
		exit;
	}

	public function cloture($token){
		$app=\Slim\Slim::getInstance();
		DBConnection::getInstance();
		$cagnotte = Cagnotte::where('token_gestion','=',$token)->first();
		$list_part = Participation::where('cagn_id','=',$cagnotte->id)->get();
		$tot=0;
		foreach($list_part as $key=>$value){
			$tot+=$value->montant;
		}
		$coffret = $cagnotte->coffret;
		$list_presta=array();
		$list_qte=array();
		$a=Appartient::where('cof_id', '=' ,$coffret->id)->get()->toArray();
		$totCoffret=0;
		foreach($a as $key){
			$list_presta[]=Prestation::find($key['pre_id']);
			$list_qte[$key['pre_id']]=$key['qte'];
		}
		foreach ($list_presta as $presta){
					foreach($a as $key=>$value){
						if($value['pre_id']==$presta['id']){
							$qte=$value['qte'];
							$totCoffret=$totCoffret+$qte*$presta['prix'];
						}
					}
		}
		if($tot>=$totCoffret){
			$cagnotte->cloture=true;
			$cagnotte->save();
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}

	public function participation($token){
		DBConnection::getInstance();
		$cagnotte = Cagnotte::where('token_participation','=',$token)->first();
		$list_part = Participation::where('cagn_id','=',$cagnotte->id)->get();
		$coffret = $cagnotte->coffret;
		$list_presta=array();
		$list_qte=array();
		$a=Appartient::where('cof_id', '=' ,$coffret->id)->get();
		foreach($a as $key){
			$list_presta[]=Prestation::find($key['pre_id']);
			$list_qte[$key['pre_id']]=$key['qte'];
		}
		$vue=new VueCagnotte($cagnotte->toArray(),$list_part->toArray(),$list_presta,$list_qte);
		$vue->render(VueCagnotte::PARTICIPATION);
	}

	public function gestion($token){
		DBConnection::getInstance();
		$cagnotte = Cagnotte::where('token_gestion','=',$token)->first();
		$list_part = Participation::where('cagn_id','=',$cagnotte->id)->get();
		$coffret = $cagnotte->coffret;
		$list_presta=array();
		$list_qte=array();
		$a=Appartient::where('cof_id', '=' ,$coffret->id)->get();
		foreach($a as $key){
			$list_presta[]=Prestation::find($key['pre_id']);
			$list_qte[$key['pre_id']]=$key['qte'];
		}
		$vue=new VueCagnotte($cagnotte->toArray(),$list_part->toArray(),$list_presta,$list_qte);
		$vue->render(VueCagnotte::GESTION);
	}

}
