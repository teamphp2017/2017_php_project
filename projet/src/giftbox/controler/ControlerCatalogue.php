<?php

namespace giftbox\controler;

use \giftbox\model\Categorie;
use \giftbox\model\Prestation;
use \giftbox\model\DBConnection;
use \giftbox\view\VueCatalogue;
use \giftbox\model\Note;
use \giftbox\controler\ControlerConnexion;

class ControlerCatalogue {

	public function index() {
		DBConnection::getInstance();
		$topPresta;
		$listCatg=Categorie::select('id')->get()->toArray();
		$listNomCatg=Categorie::select('id','nom')->get()->toArray();
		foreach($listCatg as $key=>$value){
			$listPresta=Prestation::where('cat_id','=',$value)->where('activated','!=',false)->get()->toArray();
			$listeNote=array();
			foreach($listPresta as $key=>$value){
				$listeNote[$value['id']]=ControlerCatalogue::prestationNote($value['id']);
			}
			asort($listeNote);
			$listeNote=array_reverse($listeNote,true);
			$a=array_keys($listeNote);
			$topPresta[]=array_shift($a);
		}
		$listTopPresta=array();
		foreach($topPresta as $key){
			$listTopPresta[]=Prestation::where('id','=',$key)->first()->toArray();
		}

		$vuePresta=new VueCatalogue($listTopPresta);
		$vuePresta->render(VueCatalogue::INDEX,$listNomCatg);
	}

	public function prestation($id) {
		DBConnection::getInstance();
		$listPresta = Prestation::where('id','=',$id)->get();
		$vuePresta=new VueCatalogue($listPresta->toArray());
		$vuePresta->render(VueCatalogue::ONE_PRESTATION);
	}

	public function prestations(){
		DBConnection::getInstance();
		$listPresta = Prestation::get();
		$vuePresta=new VueCatalogue($listPresta->toArray());
		$vuePresta->render(VueCatalogue::LIST_PRESTATION);
	}

	public function prestationsByCatg($id) {
		DBConnection::getInstance();
		$listPresta=Prestation::where('cat_id','=',$id)->get();
		$catg=Categorie::where('id','=',$id)->first()->nom;
		$vuePresta=new VueCatalogue($listPresta->toArray(),$id);
		$vuePresta->render(VueCatalogue::CAT_PRESTATION, $catg);
	}

	public function prestationsByCatgPrix($id) {
		DBConnection::getInstance();
		$listPresta=Prestation::where('cat_id','=',$id)->orderBy('prix')->get();
		$catg=Categorie::where('id','=',$id)->first()->nom;
		$vuePresta=new VueCatalogue($listPresta->toArray(),$id);
		$vuePresta->render(VueCatalogue::CAT_PRESTATION_PRIX, $catg);
	}

	public function prestationsByCatgPrixDesc($id) {
		DBConnection::getInstance();
		$listPresta=Prestation::where('cat_id','=',$id)->orderBy('prix','DESC')->get();
		$catg=Categorie::where('id','=',$id)->first()->nom;
		$vuePresta=new VueCatalogue($listPresta->toArray(),$id);
		$vuePresta->render(VueCatalogue::CAT_PRESTATION_PRIXDESC, $catg);
	}

	public function supprimerPrestation($id){
		DBConnection::getInstance();
		session_start();
		if(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
			Prestation::where('id','=',$id)->delete();
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}

	public function desactiverPrestation($id){
		DBConnection::getInstance();
		session_start();
		if(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
			$presta=Prestation::where('id','=',$id)->first();
			$presta->activated=false;
			$presta->save();
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}

	public function reactiverPrestation($id){
		DBConnection::getInstance();
		session_start();
		if(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
			$presta=Prestation::where('id','=',$id)->first();
			$presta->activated=true;
			$presta->save();
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}

	public function categories(){
		DBConnection::getInstance();
		$listCatg=Categorie::get();
		$vueCatg=new VueCatalogue($listCatg->toArray());
		$vueCatg->render(VueCatalogue::CATEGORIES);
	}

	public function prestationsPrix(){
		DBConnection::getInstance();
		$listPresta = Prestation::orderBy('prix')->get();
		$vuePresta=new VueCatalogue($listPresta->toArray());
		$vuePresta->render(VueCatalogue::PRIX);
	}

	public function prestationsPrixDesc(){
		DBConnection::getInstance();
		$listPresta = Prestation::orderBy('prix','DESC')->get();
		$vuePresta=new VueCatalogue($listPresta->toArray());
		$vuePresta->render(VueCatalogue::PRIXDESC);
	}

	public static function prestationNote($idPresta){
		DBConnection::getInstance();
		$liste_note=Note::where('pre_id','=',$idPresta)->get();
		$moyenne_note = 0;
		if(!sizeof($liste_note) == 0){
			foreach($liste_note as $valeur){
				$moyenne_note += $valeur->note;
			}
		}else{
			$liste_note = 0;
		}
		$note = $moyenne_note / sizeof($liste_note);
		return $note;
	}

	public function addNote($idPresta, $note){
		if($note<=5 && $note>=1){
			DBConnection::getInstance();
			$n= new Note();
			$n->pre_id=$idPresta;
			$n->note=$note;
			$n->save();
		}
		header ("Location: $_SERVER[HTTP_REFERER]"."#".$idPresta."-".$note);
		exit;
	}
}
