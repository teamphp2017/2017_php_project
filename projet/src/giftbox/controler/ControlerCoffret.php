<?php

namespace giftbox\controler;

use \giftbox\model\Categorie;
use \giftbox\model\Prestation;
use \giftbox\model\Cagnotte;
use \giftbox\model\Coffret;
use \giftbox\model\Appartient;
use \giftbox\model\DBConnection;
use \giftbox\view\VueCatalogue;
use giftbox\view\VueCoffret;
use \giftbox\controler\ControlerCatalogue;

class ControlerCoffret {

	public function addPrestation($id) {
		session_start();
		DBConnection::getInstance();
		if(Prestation::find($id) != null){
			//test si l'id existe
			if(isset($_SESSION['coffret']) && Prestation::find($id) != null){
				//si coffret existe
				if(array_key_exists($id, $_SESSION['coffret'])){
					//incrémenter la presta existante
					$_SESSION['coffret'][$id]++;
				}else{
					//créer nouvelle presta
					$_SESSION['coffret'][$id] = 1;
				}
			}else{
				//si coffret n'existe pas
				$_SESSION['coffret'] = array();
				$_SESSION['coffret'][$id] = 1;
			}
		}
		header ("Location: $_SERVER[HTTP_REFERER]");
		exit;
	}

	public function coffret($id=null) {
		session_start();
		DBConnection::getInstance();
		if(is_null($id)){
			if(isset($_SESSION['coffret'])){
				$list_presta=array();
				foreach($_SESSION['coffret'] as $key=>$value){
					$list_presta[] = Prestation::find($key);
				}
                $vue=new VueCoffret($list_presta,$_SESSION['coffret']);
                $vue->render(VueCoffret::COFFRET);
			}
			else{
								$vue=new VueCoffret();
                $vue->render(VueCoffret::COFFRET);
			}
		}
		else{
			$coffret=Coffret::where('id','=',$id)->first();
			$cadeau=array('token'=>$coffret->token,'statut'=> $coffret->opened);
			$list_presta=array();
			$list_qte=array();
			$a=Appartient::where('cof_id', '=' ,$id)->get();
			foreach($a as $key){
				$list_presta[]=Prestation::find($key['pre_id']);
				$list_qte[$key['pre_id']]=$key['qte'];
			}
			$c=Coffret::where('id','=',$id)->first();
			$co=null;
			if(!is_null($c)){
				$co=$c->toArray();
			}
			if(isset($_POST['mdp']) || is_null($co['mdp']) || isset($_SESSION['mdp'])){
				$mdp;
				if(isset($_POST['mdp'])){
					$mdp=$_POST['mdp'];
				}
				if(isset($_SESSION['mdp'])){
					$mdp=$_SESSION['mdp'];
				}
				if (is_null($co['mdp']) || password_verify($mdp, $co['mdp'])) {
					if(isset($_POST['mdp'])){
						$_SESSION['mdp']=$mdp;
					}
					$vue=new VueCoffret($list_presta,$list_qte,$cadeau);
					$vue->render(VueCoffret::COFFRETPAYE, $id);
				}
				else{
					$vue=new VueCoffret();
					$vue->render(VueCoffret::PASSWORD,$id);
				}
			}
			else{
				$vue=new VueCoffret();
				$vue->render(VueCoffret::PASSWORD,$id);
			}
		}
	}

	public function valider() {
		/*
			La validation du coffret n'est possible que si le coffret contient des prestations de au
			moins 2 catégories différentes.
			Lors de la validation, le créateur peut ajouter un message qui sera transmis au destinataire
			avec le cadeau, et choisit la modalité de paiement.
		*/
		session_start();
		DBConnection::getInstance();
		if(isset($_SESSION['coffret'])){
			if(count($_SESSION['coffret']) >= 2){
				$i=0;
				$idCatg='';
				foreach($_SESSION['coffret'] as $key=>$value){
					$list_presta[] = Prestation::find($key);
					$qte[]=$value;
					if($idCatg!=Prestation::select('cat_id')->find($key)){
						$idCatg=Prestation::select('cat_id')->find($key);
						$i++;
					}
				}
				if($i>=2){
					if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
							$nom=$_POST['nom'];
							$prenom=$_POST['prenom'];
							$email=$_POST['email'];
							if(isset($_POST['message']) && $_POST['message']!=''){
								$message=$_POST['message'];
							}
							if(isset($_POST['mdp']) && $_POST['mdp']!=''){
								$mdp=password_hash($_POST['mdp'],PASSWORD_DEFAULT);
							}
							$type=$_POST['paiement'];
							$c = new Coffret();
							$c->statut='impayé';
							$c->nom=$nom;
							$c->prenom=$prenom;
							$c->email=$email;
							if(isset($_POST['message'] ) && $_POST['message']!=''){
								$c->msg=$message;
							}
							if(isset($_POST['mdp']) && $_POST['mdp']!=''){
								$c->mdp=$mdp;
							}
							$c->type=$type;
							$c->save();
							$id=$c->id;
							$list_presta=array();
							foreach($_SESSION['coffret'] as $key=>$value){
								$a = new Appartient();
								$a->cof_id=$id;
								$a->pre_id=$key;
								$a->qte=$value;
								$a->save();
								$list_presta[] = Prestation::find($key);
							}
						if($_POST['paiement']=='classique'){
							$vue=new VueCoffret($list_presta, $_SESSION['coffret']);
							$vue->render(VueCoffret::VALIDER,$id,$type);
							unset($_SESSION['coffret']);
						}
						if($_POST['paiement']=='cagnotte'){
							$cagnotte=new Cagnotte();
							$cagnotte->coff_id=$c->id;
							$token_gestion = bin2hex(openssl_random_pseudo_bytes(16));
							$token_participation = bin2hex(openssl_random_pseudo_bytes(16));
							$cagnotte->token_gestion=$token_gestion;
							$cagnotte->token_participation=$token_participation;
							$cagnotte->cloture=false;
							$cagnotte->save();
							$controler=new ControlerCagnotte();
							unset($_SESSION['coffret']);
							$controler->linkGestion($cagnotte->token_gestion);
						}
						//echo 'VALIDE vous pouvez retrouver votre coffret ici: <a href="'.$app->urlFor('racine').'/coffret/'.$id.'"></a></br>';
					}
					else{
						$vue=new VueCoffret();
						$vue->render(VueCoffret::VALIDER);
					}
				}
				else{
					$vue=new VueCoffret();
					$vue->render(VueCoffret::ERROR);
				}
			}
			else{
				$vue=new VueCoffret();
				$vue->render(VueCoffret::ERROR);
			}
		}
		else{
			$vue=new VueCoffret();
			$vue->render(VueCoffret::ERROR);
		}
	}

	public function paiement($id) {
		/*
			S'il paie immédiatement, l'url-cadeau est générée. S'il crée une cagnotte, l'url-cagnotte est générée.
		*/
		DBConnection::getInstance();
		$c=Coffret::find($id);
		$c->statut='payé';
		$c->save();
		$vue=new VueCoffret();
		$vue->render(VueCoffret::PAIEMENT,$id);
	}

	public function modifyPassword() {

	}

	public function delPrestation($id) {
		session_start();
		DBConnection::getInstance();
		if(Prestation::find($id) != null){
			//test si l'id existe
			if(isset($_SESSION['coffret']) && Prestation::find($id) != null){
				//si coffret existe
				if($_SESSION['coffret'][$id]>1){
					$_SESSION['coffret'][$id]--;
				}else{
					$newtab=array();
					foreach($_SESSION['coffret'] as $key=>$value){
						if($key!=$id){
							$newtab[$key]=$value;
						}
					}
					$_SESSION['coffret']= $newtab;
				}
			}
		}
		header ("Location: $_SERVER[HTTP_REFERER]" );
		exit;
	}
}
