<?php

namespace giftbox\model;
	
class Cagnotte extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'cagnotte';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function coffret(){
		return $this->belongsTo('\giftbox\model\Coffret','coff_id');
	}
	
	public function participations(){
		return $this->hasMany('\giftbox\model\Participation','cagn_id');
	}
}