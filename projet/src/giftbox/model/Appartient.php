<?php

namespace giftbox\model;
	
class Appartient extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'appartient';
	public $timestamps = false;
	
	public function coffret(){
		return $this-belongsTo('\giftbox\model\Coffret','cof_id');
	}
	
	public function prestation(){
		return $this->belongsTo('\giftbox\model\Prestation','pre_id');
	}
}