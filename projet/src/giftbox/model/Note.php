<?php

namespace giftbox\model;
	
class Note extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'note';
	protected $primaryKey = 'note_id';
	public $timestamps = false;
	
	public function prestation(){
		return $this->BelongsTo('\giftbox\model\Prestation','id');
	}
}