<?php

namespace giftbox\model;
	
class Categorie extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'categorie';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	/**
	public function prestations(){
		return $this->hasMany('\giftbox\model\Prestation','cat_id');
	}**/
}