<?php

namespace giftbox\model;
	
class Prestation extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'prestation';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function categorie(){
		return $this->belongsTo('\giftbox\model\Categorie','cat_id');
	}
	
		public function note(){
		return $this->hasMany('\giftbox\model\note','idnote');
	}
}