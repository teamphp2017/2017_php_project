<?php

namespace giftbox\model;
	
class Participation extends \Illuminate\Database\Eloquent\Model{
	
	protected $table = 'participation';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function cagnotte(){
		return $this->belongsTo('\giftbox\model\Cagnotte','cagn_id');
	}
}