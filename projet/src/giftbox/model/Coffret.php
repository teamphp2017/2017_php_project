<?php

namespace giftbox\model;
	
class Coffret extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'coffret';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function appartients(){
		return $this->hasMany('\giftbox\model\Appartient','cof_id');
	}
}