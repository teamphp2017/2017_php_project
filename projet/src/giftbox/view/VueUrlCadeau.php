<?php

namespace giftbox\view;

use \giftbox\view\VueCoffret;
use \giftbox\controler\ControlerCatalogue;
use \giftbox\model\Coffret;

class VueUrlCadeau
{
	const URL_CADEAU_DEJA_ENVOYE=1;
	const URL_CADEAU_ENVOI=2;
	const URL_CADEAU_AFFICHER=3;
	const URL_CADEAU_ERREUR=4;

    private $coffret;

    function __construct($presta = null,$coffret = null)
    {
		$this->coffret=$coffret;
    }

    public function render($selecteur, $id=null, $token=null, $email=null, $listePresta=null)
    {
		$res;
		switch($selecteur)
		{
			case self::URL_CADEAU_DEJA_ENVOYE:
			$res=$this->urlCadeauDejaEnvoye($token);
			break;
			case self::URL_CADEAU_ENVOI:
			$res=$this->urlCadeauEnvoi($id, $token, $email);
			break;
			case self::URL_CADEAU_AFFICHER:
			$res=$this->urlCadeauAfficher($id, $listePresta);
			break;
			case self::URL_CADEAU_ERREUR:
			$res=$this->urlCadeauErreur();
			break;
		}
        $app=\Slim\Slim::getInstance();
        $routecss=$app->urlFor('css');
		$h=new VueHeader();
		$header=$h->headerToHtml();
        $html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="$routecss/style.css">
		<title>GiftBox</title>
		<link rel="icon" type="image/png" href="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" />
		<script>
			function next(i){
				if(document.getElementById(i+1) != null){
					document.getElementById(i).style.display="none";
					document.getElementById(i+1).style.display="block";
				}
				else{
					document.getElementById("first").style.display="none";
					document.getElementById("second").style.display="block";
				}
			}
		</script>
	</head>
	<body>
		$header
		<div class="content">
		$res
		</div>
	</body>
</html>
EOT;
        echo $html;
    }


	public function urlCadeauDejaEnvoye($token){
		$app=\Slim\Slim::getInstance();
		return '<h3>Ce coffret cadeau à déjà été envoyé par email !</h3><br><h5>Voici le lien du coffret cadeau: <a href="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('cadeau', array('token'=>$token)).'">&#127873;</a></h5>
							<p>Attention ! Si vous cliquez sur le lien, le lien sera considéré comme ouvert sur la page de votre coffret</p>';
	}

	public function urlCadeauEnvoi($id, $token, $email){
		$app=\Slim\Slim::getInstance();
		return '<h3>Merci ! Votre coffret cadeau à bien été envoyé à l\'email suivante: '.$email.'</h3><br><h5>Voici le lien du coffret cadeau: <a href="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('cadeau', array('token'=>$token)).'">&#127873;</a></h5>
							<p>Attention ! Si vous cliquez sur le lien, le lien sera considéré comme ouvert sur la page de votre coffret</p>';
	}

	public function urlCadeauAfficher($idCoffret, $listePresta){
		$app=\Slim\Slim::getInstance();
		$res='<div id="first"><h3>On vous à offert : </h3>';
		$i=0;
		foreach($listePresta as $value)
		{
			$res.=$this->prestationToHtmlAlone($value,$i);
			$i++;
		}
		$res.='<h5 style="text-align: center;">(Cliquer sur la prestation pour passer au cadeau suivant)</h5></div>';
		$res.='<div id="second" style="display: none;">
		<h3>Voici le contenu de votre superbe coffret cadeau !</h3>
		<table>
		<thead>
			<tr>
				<th>Aperçu</th>
				<th>Nom & Description</th>
			</tr>
		</thead>';

		foreach($listePresta as $value)
		{
			$res.=$this->prestationToHtml($value);
		}
		$res.='</table></div>';
		return $res;
	}

	private function prestationToHtml($value)
	{
		$app=\Slim\Slim::getInstance();
		return '<tr>
		<td><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
		<td><h6>' . $value['nom']
		.'</h6>'.$value ['descr'] . '</td></tr>';
	}

	private function prestationToHtmlAlone($value,$i)
	{
		$app=\Slim\Slim::getInstance();
		$res='<table onClick="next('.$i.')" id="'.$i.'" style="border-style: solid;';
		if($i!=0){
			$res.='display:none; ';
		}
		$res.='"><tr>
			<th class="noHighlight" colspan="2"><h6>'.$value['nom'].'</h6></th>
		</tr>
		<tr>
			<td class="noHighlight rowspan="4"><img style="width: 300px;" class="alone" src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
			<td class="noHighlight"><h5>Description :</h5>'.$value ['descr'] . '</td>
		</tr>
		</table>';
		return $res;
	}

	public function urlCadeauErreur(){
		return '<h3>Erreur, ce coffret cadeau n\'existe pas !</h3>';
	}


}
