<?php

namespace giftbox\view;

class VueCagnotte{

	const PARTICIPATION=1;
	const GESTION=2;

	private $cagnotte,$list_part,$list_prest,$list_app;

	function __construct($cagnotte,$list_part,$list_prest,$list_app){
		$this->cagnotte=$cagnotte;
		$this->list_part=$list_part;
		$this->list_prest=$list_prest;
		$this->list_app=$list_app;
    }


	private function cagnotteToHtml($selecteur){
		$app=\Slim\Slim::getInstance();
		switch($selecteur)
		{
			case self::PARTICIPATION:
				$res='<table>
					<thead>
						<tr>
							<th colspan="6"><h2>Cagnotte</h2></th>
						</tr>
						<tr>
							<th>Aperçu</th>
							<th>Nom & Description</th>
							<th>Note</th>
							<th>Prix</th>
							<th>Quantité</th>
						</tr>
					</thead>';
				$tot=0;
				foreach ($this->list_prest as $presta)
					{
						foreach($this->list_app as $key=>$value){
							if($key==$presta['id']){
								$qte=$value;
								$tot=$tot+$qte*$presta['prix'];
							}
						}
						$res.=$this->prestationToHtml($presta,$qte);
					}
				$res.='</table>
						<p class="prix">Montant à atteindre : '.$tot.'€</p>';
				$res.='<p class="prix">Montant récolté : '.$this->sommeParticipation().'€ </p>';
				if($this->cagnotte['cloture']==false){
					$res.='<form action="'.$app->urlFor('newMontant',array('token'=>$this->cagnotte['token_participation'])).'" method="post">
								Nom : <input type="text" name="nom"></input>
								Prénom : <input type="text" name="prenom"></input>
								Montant : <input type="number" name="montant"></input>
								<input class="ok" type="submit" value="Valider"></input>
							</form>';
				}
				else{
					$res.='<p>Cagnotte cloturée</p>';
				}
				return $res;
			break;
			case self::GESTION:
				$res='<table>
					<thead>
						<tr>
							<th colspan="6"><h2>Cagnotte</h2></th>
						</tr>
						<tr>
							<th>Aperçu</th>
							<th>Nom & Description</th>
							<th>Note</th>
							<th>Prix</th>
							<th>Quantité</th>
						</tr>
					</thead>';
				$tot=0;
				$qte=0;
				foreach ($this->list_prest as $presta)
					{
						foreach($this->list_app as $key=>$value){
							if($key==$presta['id']){
								$qte=$value;
								$tot=$tot+$qte*$presta['prix'];
							}
						}
						$res.=$this->prestationToHtml($presta,$qte);
					}
				$res.='</table>
						<p class="prix">Montant à atteindre : '.$tot.'€</p>';
				$res.='<p class="prix">Montant récolté : '.$this->sommeParticipation().'€ </p>';
				if($this->cagnotte['cloture']==true){
					$res.='<a href="'.$app->urlFor('paiement',array('id'=>$this->cagnotte['coff_id'])).'"><input class="ok" type="submit" value="Valider et Payer"></a>';
				}
				if($this->cagnotte['cloture']==false){
					$res.='<form action="'.$app->urlFor('newMontant',array('token'=>$this->cagnotte['token_participation'])).'" method="post">
								Montant : <input type="number" name="montant"></input>
								<input class="ok" type="submit" value="Valider"></input>
							</form>';
				}
				if($this->sommeParticipation()>=$tot && $this->cagnotte['cloture']==false){
					$res.='<form action="'.$app->urlFor('cloture',array('token'=>$this->cagnotte['token_gestion'])).'" method="post">
									<input class="ok" type="submit" value="Cloturer"></input>
								 </form>';
				}
				$res.='<p>URL gestion</p><input type="text" value="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('gestion',array('token'=>$this->cagnotte['token_gestion'])).'" />';
				$res.='<p>URL participation : </p><input type="text" value="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('participation',array('token'=>$this->cagnotte['token_participation'])).'"/>';
				return $res;
			break;
		}
	}

	private function prestationToHtml($value,$qte){
		$app=\Slim\Slim::getInstance();
			$res='<tr>
			<td><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
			<td><h6>' . $value['nom']
			.'</h6>'.$value ['descr'] . '</td>
			<td></td>
			<td class="prix">' . $value ['prix'].'€</td>'.
			'<td class="qte">'.$qte.'</td></tr>';
		return $res;
    }

	private function sommeParticipation(){
		$tot=0;
		foreach($this->list_part as $part){
			$tot=$tot+$part['montant'];
		}
		return $tot;
	}

	public function render($selecteur){
		$res;
		switch($selecteur)
		{
			case self::PARTICIPATION:
			$res=$this->cagnotteToHtml($selecteur);
			break;
			case self::GESTION:
			$res=$this->cagnotteToHtml($selecteur);
			break;
		}
        $app=\Slim\Slim::getInstance();
        $routecss=$app->urlFor('css');
		$h=new VueHeader();
		$header=$h->headerToHtml();
        $html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="$routecss/style.css">
		<title>GiftBox</title>
		<link rel="icon" type="image/png" href="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" />
	</head>
	<body>
		$header
		<div class="content">
		$res
		</div>
	</body>
</html>
EOT;
        echo $html;
    }
}
