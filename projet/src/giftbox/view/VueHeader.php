<?php

namespace giftbox\view;

use \giftbox\controler\ControlerConnexion;

class VueHeader{

	private $location;

	function __construct(){

	}

	function headerToHtml(){
		$app=\Slim\Slim::getInstance();
		if(!isset($_SESSION)){
			session_start();
		}
		if(isset($_SESSION['coffret'])){
			$tailleCoffret=sizeof($_SESSION['coffret']);
		}
		else{
			$tailleCoffret=0;
		}
		if(!isset($_SESSION['isConnected']) || $_SESSION['isConnected']==false){
			$res='<header>
			<div class="loginBar">
				<ul>
					<form method="post" action="'.$app->urlFor('login').'">
					<li>Login : <input type="text" name="login"/></li>
					<li>Password : <input type="password" name="password"/></li>
					<li><input class="login" type="submit" value="Connexion"/></li>
					</form>
				</ul>
			</div>
					<div id="logo">
						<a href="'.$app->urlFor('racine').'">
		                    <img src="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" alt="Logo" />
							<h1>GiftBox</h1>
						</a>
					</div>
					<nav>
						<ul>
							<li><a href="'.$app->urlFor('prestations').'">Catalogue</a></li>
							<li><a href="'.$app->urlFor('categories').'"> Categories</a></li>
							<li><a href="'.$app->urlFor('coffret').'">Coffret ('.$tailleCoffret.')</a></li>
						</ul>
					</nav>
				  </header>';
		}
		if(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true){
			if(ControlerConnexion::verify()){
				$res='<header>
				<div class="loginBar">
					<ul>
						<form method="post" action="'.$app->urlFor('logout').'">
						<li><input class="login" type="submit" value="Déconnexion"/></li>
						</form>
					</ul>
				</div>
						<div id="logo">
							<a href="'.$app->urlFor('racine').'">
													<img src="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" alt="Logo" />
								<h1>GiftBox</h1>
							</a>
						</div>
						<nav>
							<ul>
								<li><a href="'.$app->urlFor('prestations').'">Catalogue</a></li>
								<li><a href="'.$app->urlFor('categories').'"> Categories</a></li>
								<li><a href="'.$app->urlFor('coffret').'">Coffret ('.$tailleCoffret.')</a></li>
							</ul>
						</nav>
				</header>';
			}
			else{
				$res='<header>
				<div class="loginBar">
					<ul>
						<form method="post" action="'.$app->urlFor('login').'">
						<li>Login : <input type="text" id="login"/></li>
						<li>Password : <input type="password" id="password"/></li>
						<li><input class="login" type="submit" value="Connexion"/></li>
						</form>
					</ul>
				</div>
						<div id="logo">
							<a href="'.$app->urlFor('racine').'">
													<img src="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" alt="Logo" />
								<h1>GiftBox</h1>
							</a>
						</div>
						<nav>
							<ul>
								<li><a href="'.$app->urlFor('prestations').'">Catalogue</a></li>
								<li><a href="'.$app->urlFor('categories').'"> Categories</a></li>
								<li><a href="'.$app->urlFor('coffret').'">Coffret ('.$tailleCoffret.')</a></li>
							</ul>
						</nav>
				 </header>';
			}
		}

		return $res;
	}

}
