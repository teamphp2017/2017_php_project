<?php

namespace giftbox\view;

class VueCoffret
{
	const COFFRET=1;
	const VALIDER=2;
	const ERROR=3;
	const PAIEMENT=4;
	const COFFRETPAYE=5;
	const PASSWORD=6;

	private $location;
  private $coffret;
	private $presta;
	private $cadeau;

    function __construct($presta = null,$coffret = null,$cadeau=null)
    {
        $this->coffret=$coffret;
				$this->presta=$presta;
				$this->cadeau=$cadeau;
    }

    private function coffretToHtml($mode=null, $id=null)
    {
		if(!is_null($this->presta)&&sizeof($this->presta)!=0){
			$app=\Slim\Slim::getInstance();
			$res='';
			if($mode=='paye'){
				$res.='<p>Gestion de votre coffret :</p>
				<table>
			<thead>
				<tr>
					<th colspan="6"><h2>Coffret</h2></th>
				</tr>
				<tr>
					<th>Aperçu</th>
					<th>Nom & Description</th>
					<th>Note</th>
					<th>Prix</th>
					<th>Quantité</th>
				</tr>
			</thead>';
			}
			else{
			$res.='<table>
			<thead>
				<tr>
					<th colspan="6"><h2>Coffret</h2></th>
				</tr>
				<tr>
					<th>Aperçu</th>
					<th>Nom & Description</th>
					<th>Note</th>
					<th>Prix</th>
					<th>Quantité</th>
					<th>Action</th>
				</tr>
			</thead>';
			}
			$tot=0;
			foreach ($this->presta as $presta)
			{
				foreach($this->coffret as $key=>$value){
					if($key==$presta['id']){
						$qte=$value;
						$tot=$tot+$qte*$presta['prix'];
					}
				}
				$res.=$this->prestationToHtml($presta,$qte,$mode);
			}
			$res.='</table>
			<p class="prix">Total : '.$tot.'€</p>';

			//Boutton valider et payer
			if($mode == ''){
				$res.='<br>
				<a href="'.$app->urlFor('valider').'"><input class="ok" type="submit" value="Valider et Payer">';
			}
			if($mode == 'paye'){
				if(is_null($this->cadeau['token'])){
					$res.='<br>
					<form method="post" action="'.$app->urlFor('urlCadeau', array('id'=>$id)).'">
						<input class="normal" type="email" name="email"/>
						<input class="ok" type="submit" value="Envoyer url cadeau"/>
					</form>';
				}
				else{
					if($this->cadeau['statut']==true){
						$res.='<p>Votre coffret a bien été reçu et ouvert !</p>
											<br><h5>Voici le lien du coffret cadeau: 	<a href="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('cadeau', array('token'=>$this->cadeau['token'])).'">&#127873;</a></h5>';
					}
					else{
						$res.='<p>Le coffret n\'a pas encore été ouvert</p>
								<br><h5>Voici le lien du coffret cadeau: <a href="https://webetu.iutnc.univ-lorraine.fr'.$app->urlFor('cadeau', array('token'=>$this->cadeau['token'])).'">&#127873;</a></h5>
										 <p>Attention ! Si vous cliquez sur le lien, le lien sera considéré comme ouvert sur la page de votre coffret</p>';
					}
				}
			}
		}
		else{
			$res='Vous n\'avez pas encore sélectionné d\'article !';
		}
        return $res;

    }

	private function valider($id=null, $type=null)
	{
		$app=\Slim\Slim::getInstance();
		$res='';
		if(!is_null($id)){
			$res.='<p>Résumé de la commande :</p>';
			$res.=$this->coffretToHtml($type);
			if($type=='classique'){
				$res.='</br>
					<a href="'.$app->urlFor('paiement',array('id'=>$id)).'"><input class="ok" type="submit" value="Passer au paiement">';
			}
			if($type=='cagnotte'){

			}
		}
		else{
			$res='<table>
				<form action="'.$app->urlFor('validerP').'" method="post">
					<tr>
						<td class="noHighlight">Nom* : </td>
						<td class="noHighlight"><input class="normal" type="text" name="nom"/></td>
					</tr>
					<tr>
						<td class="noHighlight">Prénom* : </td>
						<td class="noHighlight"><input class="normal" type="text" name="prenom"/></td>
					</tr>
					<tr>
						<td class="noHighlight">Email* : </td>
						<td class="noHighlight"><input class="normal" type="email" name="email"/></td>
					</tr>
					<tr>
						<td class="noHighlight">Message : </td>
						<td class="noHighlight"><textarea class="normal" placeholder="Message à joindre avec le coffret" type="text" name="message" rows="10" cols="50"></textarea></td>
					</tr>
					<tr>
						<td  class="noHighlight">Mot de Passe : </td>
						<td class="noHighlight"><input class="normal" type="password" name="mdp"/></td>
					</tr>
					<tr>
						<td class="noHighlight">Paiement : </td>
						<td class="noHighlight"><input type="radio" name="paiement" value="classique" checked /> <label for="classique">Classique</label>
								  <input type="radio" name="paiement" value="cagnotte" /> <label for="cagnotte">Cagnotte</label></td>
					</tr>
					<tr>
						<td class="noHighlight"><input class="ok" type="submit" value="Suivant"/></td>
						<td class="noHighlight"></td>
					</tr>
				  </form>
				 </table>';
		}
		return $res;
	}

	private function error()
	{
		return 'Le coffret ne contient pas plus de 2 éléments d\'au moins 2 catégories différentes et ne peut être validé</br>';
	}

    private function prestationToHtml($value,$qte,$mode=null)
    {
		$app=\Slim\Slim::getInstance();
		$res='';
		if(is_null($mode)){
			$res.='<tr>
			<td><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
			<td><h6>' . $value['nom']
			.'</h6>'.$value ['descr'] . '</td>
			<td></td>
			<td class="prix">' . $value ['prix'].'€</td>'.
			'<td class="qte">'.$qte.'</td>'.
			'<td class="action"><a href="'.$app->urlFor('supprimerPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Supprimer" type="submit" value="Supprimer"></input></a></td></tr>';
		}
		else{
			$res.='<tr>
			<td><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
			<td><h6>' . $value['nom']
			.'</h6>'.$value ['descr'] . '</td>
			<td></td>
			<td class="prix">' . $value ['prix'].'€</td>'.
			'<td class="qte">'.$qte.'</td></tr>';
		}
		return $res;
    }

	private function paiement($id){
		$app=\Slim\Slim::getInstance();
		return '<p>Paiement accepté! Votre coffret est disponible à l\'adresse suivante :</p>
		<a href="'.$app->urlFor('coffretBD',array('id'=>$id)).'">'.$app->urlFor('coffretBD',array('id'=>$id)).'</a>';
	}

	private function password($id){
		$app=\Slim\Slim::getInstance();
		return '<form method="post" action="'.$app->urlFor('coffretBDPost',array('id'=>$id)).'">
					<input class="normal" type="password" name="mdp"/>
					<input class="ok" type="submit" value="OK"/>
				</form>';
	}

    public function render($selecteur, $id=null, $type=null)
    {
		$res;
		switch($selecteur)
		{
			case self::COFFRET:
			$res=$this->coffretToHtml();
			break;
			case self::VALIDER:
				if(is_null($id)){
					$res=$this->valider();
				}
				else{
					$res=$this->valider($id, $type);
				}
			break;
			case self::ERROR:
			$res=$this->error();
			break;
			case self::PAIEMENT:
			$res=$this->paiement($id);
			break;
			case self::COFFRETPAYE:
			$res=$this->coffretToHtml('paye', $id);
			break;
			case self::PASSWORD:
			$res=$this->password($id);
			break;
		}
        $app=\Slim\Slim::getInstance();
        $routecss=$app->urlFor('css');
		$h=new VueHeader();
		$header=$h->headerToHtml();
        $html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="$routecss/style.css">
		<title>GiftBox</title>
		<link rel="icon" type="image/png" href="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" />
	</head>
	<body>
		$header
		<div class="content">
		$res
		</div>
	</body>
</html>
EOT;
        echo $html;
    }
}
