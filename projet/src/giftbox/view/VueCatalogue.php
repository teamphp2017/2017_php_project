<?php

namespace giftbox\view;
use \giftbox\view\VueHeader;
use \giftbox\controler\ControlerCatalogue;
use \giftbox\controler\ControlerConnexion;

class VueCatalogue
{
	const INDEX=0;
	const LIST_PRESTATION=1;
	const ONE_PRESTATION=2;
	const CAT_PRESTATION=3;
	const CATEGORIES=4;
	const PRIX=5;
	const PRIXDESC=6;
	const CAT_PRESTATION_PRIX=7;
	const CAT_PRESTATION_PRIXDESC=8;

	private $tab,$catid;
	function __construct($liste, $catid=null)
	{
		$this->tab=$liste;
		$this->catid=$catid;
	}

	private function listeToHtml($order=null)
	{
		$app=\Slim\Slim::getInstance();
		$res='
		<table>
		<thead>
			<tr>
				<th colspan="5"><h2>Prestations</h2></th>
			</tr>
			<tr>
				<th>Aperçu</th>
				<th>Nom & Description</th>
				<th>Note</th>';
		if(is_null($order)){
			$res.='<th><a href="'.$app->urlFor('prix').'">Prix</a></th>';
		}
		else{
			if($order=='DESC'){
				$res.='<th><a href="'.$app->urlFor('prix').'">Prix ↑</a></th>';
			}
			else{
				$res.='<th><a href="'.$app->urlFor('prixdesc').'">Prix ↓</a></th>';
			}
		}
			$res.='<th>Action</th>
			</tr>
		</thead>';
		foreach($this->tab as $value)
		{
			$res.=$this->prestationToHtml($value);
		}
		if(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
			$res.='<tr>
							<form method="post" action="">
							<td></td>
							<td>Titre : </br><input name="title" type="text" id="title"/></br>
									Description : </br><textarea rows="4" cols="40" name="desc"></textarea></td>
							<td></td>
							<td>Prix : <input name="prix" type="number" id="prix"/></td>
							<td><input class="ok" type="submit" name="submit" id="submit" value="Envoyer"/></td>
							</form>
						</tr>';
		}
		$res.='</table>';
		return $res;
	}
	/**
	* prend l'attribut tableau contenant une catégorie et affiche la liste des prestations de cette catégorie
	*/
	private function prestationsFromCategorie($catg,$order=null)
	{
		$app=\Slim\Slim::getInstance();
		$res='
		<table>
		<thead>
			<tr>
				<th colspan="5"><h2>'.$catg.'</h2></th>
			</tr>
			<tr>
				<th>Aperçu</th>
				<th>Nom & Description</th>
				<th>Note</th>';
		if(is_null($order)){
			$res.='<th><a href="'.$app->urlFor('categorieprix',array('id'=>$this->catid)).'">Prix </a></th>';
		}
		else{
			if($order=='DESC'){
				$res.='<th><a href="'.$app->urlFor('categorieprix',array('id'=>$this->catid)).'">Prix ↑</a></th>';
			}
			else{
				$res.='<th><a href="'.$app->urlFor('categorieprixdesc',array('id'=>$this->catid)).'">Prix ↓</a></th>';
			}
		}
			$res.='<th>Action</th>
			</tr>
		</thead>';
		foreach($this->tab as $value)
		{
			$res.=$this->prestationToHtml($value);
		}
		$res.='</table>';
		return $res;
	}

	private function prestationToHtml($value)
	{
		if(!isset($_SESSION)){
			session_start();
		}
		$app=\Slim\Slim::getInstance();
		$img_note = $this->getNote($value['id']);
		if((!isset($_SESSION['isConnected']) || $_SESSION['isConnected']==false) && $value['activated']==true){
			return '<tr>
			<td class="imgPrest"><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
			<td class="pointer" onclick="document.location=\''.$app->urlFor('prestation',array('id'=>$value['id'])).'\'"><h6><a href="'.$app->urlFor('prestation',array('id'=>$value['id'])).'">' . $value['nom']
			.'</a></h6>'.$value ['descr'] . '</td>
			<td class="note">'.$img_note.'</td>
			<td class="prix">' . $value ['prix'].'€</td>
			<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></td></tr>';
		}
		elseif(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
			if($value['activated']==true){
				return '<tr>
				<td class="imgPrest"><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
				<td class="pointer" onclick="document.location=\''.$app->urlFor('prestation',array('id'=>$value['id'])).'\'"><h6><a href="'.$app->urlFor('prestation',array('id'=>$value['id'])).'">' . $value['nom']
				.'</a></h6>'.$value ['descr'] . '</td>
				<td class="note">'.$img_note.'</td>
				<td class="prix">' . $value ['prix'].'€</td>
				<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></br>
													 <a href="'.$app->urlFor('desactiverPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Désactiver" type="submit" value="Désactiver"></input></a></br>
													 <a href="'.$app->urlFor('supprPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Supprimer" type="submit" value="Supprimer"></input></a>
				</td>
				</tr>';
			}
			else{
				return '<tr>
				<td class="imgPrest"><img src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
				<td class="pointer" onclick="document.location=\''.$app->urlFor('prestation',array('id'=>$value['id'])).'\'"><h6><a href="'.$app->urlFor('prestation',array('id'=>$value['id'])).'">' . $value['nom']
				.'</a></h6>'.$value ['descr'] . '</td>
				<td class="note">'.$img_note.'</td>
				<td class="prix">' . $value ['prix'].'€</td>
				<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></br>
													 <a href="'.$app->urlFor('reactiverPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Réactiver" type="submit" value="Réactiver"></input></a></br>
													 <a href="'.$app->urlFor('supprPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Supprimer" type="submit" value="Supprimer"></input></a>
				</td>
				</tr>';
			}
		}
	}

	private function getNote($numPresta){
		$app=\Slim\Slim::getInstance();
		$note = ControlerCatalogue::prestationNote($numPresta);
		$res='';
		for($i=1; $i<6; $i++){
			if($i<=$note){
				$res .= '<a href="'.$app->urlFor('newNote',array('id'=>$numPresta,'note'=>$i)).'"><img onMouseOver="changeimage('.$numPresta.','.$i.')" onMouseOut="changeimagedefault('.$numPresta.','.$i.')" id="'.$numPresta.'-'.$i.'" class="note_star" alt="filled" src="'.$app->urlFor('racine').'img/filled_star.png"/></a>';
			}else{
				$res .= '<a href="'.$app->urlFor('newNote',array('id'=>$numPresta,'note'=>$i)).'"><img onMouseOver="changeimage('.$numPresta.','.$i.')" onMouseOut="changeimagedefault('.$numPresta.','.$i.')" id="'.$numPresta.'-'.$i.'" class="note_star" alt="empty" src="'.$app->urlFor('racine').'img/empty_star.png"/></a>';
			}
		}
		return $res;
	}

	private function prestationToHtmlAlone($value)
	{
		if(!isset($_SESSION)){
			session_start();
		}
		$app=\Slim\Slim::getInstance();
		if((!isset($_SESSION['isConnected']) || $_SESSION['isConnected']==false) && $value['activated']==true){
			return '<table>
			<tr>
				<th class="noHighlight" colspan="2"><h6>'.$value['nom'].'</h6></th>
			</tr>
			<tr>
				<td rowspan="4"><img class="alone" src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
				<td class="noHighlight"><h5>Description :</h5>'.$value ['descr'] . '</td>
			</tr>
			<tr>
				<td class="prix noHighlight">'. $value ['prix'].'€</td>
			</tr>
			<tr>
				<td class="noHighlight"><h5>Note: </h5>'.$this->getNote($value['id']).'</td>
			</tr>
			<tr>
			<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></br></td>
			</tr>
			</table>';
		}
			elseif(isset($_SESSION['isConnected']) && $_SESSION['isConnected']==true && ControlerConnexion::verify()){
				if($value['activated']==true){
					return '<table>
					<tr>
						<th class="noHighlight" colspan="2"><h6>'.$value['nom'].'</h6></th>
					</tr>
					<tr>
						<td rowspan="4"><img class="alone" src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
						<td class="noHighlight"><h5>Description :</h5>'.$value ['descr'] . '</td>
					</tr>
					<tr>
						<td class="prix noHighlight">'. $value ['prix'].'€</td>
					</tr>
					<tr>
						<td class="noHighlight"><h5>Note: </h5>'.$this->getNote($value['id']).'</td>
					</tr>
					<tr>
					<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></br>
														 <a href="'.$app->urlFor('desactiverPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Désactiver" type="submit" value="Désactiver"></input></a></br>
														 <a href="'.$app->urlFor('supprPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Supprimer" type="submit" value="Supprimer"></input></a>
					</td>
					</tr>
					</table>';
				}
				else{
					return '<table>
					<tr>
						<th class="noHighlight" colspan="2"><h6>'.$value['nom'].'</h6></th>
					</tr>
					<tr>
						<td rowspan="4"><img class="alone" src="'.$app->urlFor('racine').'img/' . $value ['img'] . '" /></td>
						<td class="noHighlight"><h5>Description :</h5>'.$value ['descr'] . '</td>
					</tr>
					<tr>
						<td class="prix noHighlight">'. $value ['prix'].'€</td>
					</tr>
					<tr>
						<td class="noHighlight"><h5>Note: </h5>'.$this->getNote($value['id']).'</td>
					</tr>
					<tr>
					<td class="action"><a href="'.$app->urlFor('ajouterPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Ajouter au coffret" type="submit" value="Ajouter au Panier"></input></a></br>
														 <a href="'.$app->urlFor('reactiverPrestation',array('id'=>$value['id'])).'"><input class="ok" title="reactiver" type="submit" value="reactiver"></input></a></br>
														 <a href="'.$app->urlFor('supprPrestation',array('id'=>$value['id'])).'"><input class="ok" title="Supprimer" type="submit" value="Supprimer"></input></a>
					</td>
					</tr>
					</table>';
				}
			}
		}

	private function categories()
	{
		$app=\Slim\Slim::getInstance();
		$res='<table>';
		foreach($this->tab as $value){
			$res.='<tr class="pointer categorie" onclick="document.location=\''.$app->urlFor('categorie',array('id'=>$value['id'])).'\'">
						<td><h6><a href="'.$app->urlFor('categorie',array('id'=>$value['id'])).'">'.$value['nom'].'</a></h6></td>
				   </tr>';
		}
		$res.='</table>';
		return $res;
	}

	private function index($list){
		$app=\Slim\Slim::getInstance();
		$res='<table>
				<tr>
					<th class="noHighlight welcome" rowspan="4"><h3>Bienvenue</h3>sur GiftBox</th>
					<th><h4>Nos prestations à l\'honneur</h4></th>
				</tr>
				<tr>
					<td class="noHighlight"><table>';
		$i=0;
		foreach($this->tab as $value){
			$res.='<tr class="pointer categorie" onclick="document.location=\''.$app->urlFor('categorie',array('id'=>$list[$i]['id'])).'\'"><td colspan="5" ><h6>'.$list[$i]['nom'].'</h6></td></tr>';
			$res.=$this->prestationToHtml($value);
			$i++;
		}
		$res.='</table>
		</td>
				</tr>
			  </table>';
		return $res;
	}

	public function render($selecteur, $catg=null)
	{
		$res;
		switch($selecteur)
		{
			case self::INDEX:
			$res=$this->index($catg);
			break;
			case self::LIST_PRESTATION:
			$res=$this->listeToHtml();
			break;
			case self::ONE_PRESTATION:
			$res=$this->prestationToHtmlAlone($this->tab[0]);
			break;
			case self::CAT_PRESTATION:
			$res=$this->prestationsFromCategorie($catg);
			break;
			case self::CATEGORIES:
			$res=$this->categories();
			break;
			case self::PRIX:
			$res=$this->listeToHtml('NORMAL');
			break;
			case self::PRIXDESC:
			$res=$this->listeToHtml('DESC');
			break;
			case self::CAT_PRESTATION_PRIX:
			$res=$this->prestationsFromCategorie($catg,'NORMAL');
			break;
			case self::CAT_PRESTATION_PRIXDESC:
			$res=$this->prestationsFromCategorie($catg,'DESC');
			break;
		}
		$app=\Slim\Slim::getInstance();
		$routecss=$app->urlFor('css');
		$h=new VueHeader();
		$header=$h->headerToHtml();
		$script='<script type="text/javascript">
			function changeimage(numPresta,id){
				for(var iter=id; iter<=5;iter++){
					i=numPresta+\'-\'+iter;
					document.getElementById(i).src=\''.$app->urlFor('racine').'img/empty_star.png\';
				}
				for(var iter=1 ;iter<=id;iter++){
					i=numPresta+\'-\'+iter;
					document.getElementById(i).src=\''.$app->urlFor('racine').'img/filled_star.png\';
				}
			}
			function changeimagedefault(numPresta,id){
				for(var iter=1; iter<=5;iter++){
					i=numPresta+\'-\'+iter;
					if(document.getElementById(i).alt==\'empty\'){
						document.getElementById(i).src=\''.$app->urlFor('racine').'img/empty_star.png\';
					}
					else{
						document.getElementById(i).src=\''.$app->urlFor('racine').'img/filled_star.png\';
					}
				}
			}
		</script>';
		$html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>GiftBox</title>
		<link rel="icon" type="image/png" href="http://besticons.net/sites/default/files/gold-gift-box-icon-3362.png" />
		<link rel="stylesheet" href="$routecss/style.css">
		$script
	</head>
	<body>
	$header
	<div class="content">
	$res
	</div>
	</body>
</html>
EOT;

		echo $html;
	}
}
