SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `prestation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  `descr` text NOT NULL,
  `cat_id` int(11) NOT NULL,
  `img` text NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `activated` boolean NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

INSERT INTO `prestation` (`id`, `nom`, `descr`, `cat_id`, `img`, `prix`,`activated`) VALUES
(1, 'Champagne', 'Bouteille de champagne + flutes + jeux � gratter', 1, 'champagne.jpg', '20.00',true),
(2, 'Musique', 'Partitions de piano � 4 mains', 1, 'musique.jpg', '25.00',true),
(3, 'Exposition', 'Visite guid�e de l�exposition �REGARDER� � la galerie Poirel', 2, 'poirelregarder.jpg', '14.00',true),
(4, 'Go�ter', 'Go�ter au FIFNL', 3, 'gouter.jpg', '20.00',true),
(5, 'Projection', 'Projection courts-m�trages au FIFNL', 2, 'film.jpg', '10.00',true),
(6, 'Bouquet', 'Bouquet de roses et Mots de Marion Renaud', 1, 'rose.jpg', '16.00',true),
(7, 'Diner Stanislas', 'Diner � La Table du Bon Roi Stanislas (Ap�ritif /Entr�e / Plat / Vin / Dessert / Caf� / Digestif)', 3, 'bonroi.jpg', '60.00',true),
(8, 'Origami', 'Baguettes magiques en Origami en buvant un th�', 3, 'origami.jpg', '12.00',true),
(9, 'Livres', 'Livre bricolage avec petits-enfants + Roman', 1, 'bricolage.jpg', '24.00',true),
(10, 'Diner  Grand Rue ', 'Diner au Grand�Ru(e) (Ap�ritif / Entr�e / Plat / Vin / Dessert / Caf�)', 3, 'grandrue.jpg', '59.00',true),
(11, 'Visite guid�e', 'Visite guid�e personnalis�e de Saint-Epvre jusqu�� Stanislas', 2, 'place.jpg', '11.00',true),
(12, 'Bijoux', 'Bijoux de manteau + Sous-verre pochette de disque + Lait apr�s-soleil', 1, 'bijoux.jpg', '29.00',true),
(13, 'Op�ra', 'Concert comment� � l�Op�ra', 2, 'opera.jpg', '15.00',true),
(14, 'Th� Hotel de la reine', 'Th� de debriefing au bar de l�Hotel de la reine', 3, 'hotelreine.gif', '5.00',true),
(15, 'Jeu connaissance', 'Jeu pour faire connaissance', 2, 'connaissance.jpg', '6.00',true),
(16, 'Diner', 'Diner (Ap�ritif / Plat / Vin / Dessert / Caf�)', 3, 'diner.jpg', '40.00',true),
(17, 'Cadeaux individuels', 'Cadeaux individuels sur le th�me de la soir�e', 1, 'cadeaux.jpg', '13.00',true),
(18, 'Animation', 'Activit� anim�e par un intervenant ext�rieur', 2, 'animateur.jpg', '9.00',true),
(19, 'Jeu contacts', 'Jeu pour �change de contacts', 2, 'contact.png', '5.00',true),
(20, 'Cocktail', 'Cocktail de fin de soir�e', 3, 'cocktail.jpg', '12.00',true),
(21, 'Star Wars', 'Star Wars - Le R�veil de la Force. S�ance cin�ma 3D', 2, 'starwars.jpg', '12.00',true),
(22, 'Concert', 'Un concert � Nancy', 2, 'concert.jpg', '17.00',true),
(23, 'Appart Hotel', 'Appart�h�tel Coeur de Ville, en plein centre-ville', 4, 'apparthotel.jpg', '56.00',true),
(24, 'H�tel d''Haussonville', 'H�tel d''Haussonville, au coeur de la Vieille ville � deux pas de la place Stanislas', 4, 'hotel_haussonville_logo.jpg', '169.00',true),
(25, 'Boite de nuit', 'Discoth�que, Bo�te tendance avec des soir�es � th�me & DJ invit�s', 2, 'boitedenuit.jpg', '32.00',true),
(26, 'Plan�tes Laser', 'Laser game : Gilet �lectronique et pistolet laser comme mat�riel, vous voil� �quip�.', 2, 'laser.jpg', '15.00',true),
(27, 'Fort Aventure', 'D�couvrez Fort Aventure � Bainville-sur-Madon, un site Accropierre unique en Lorraine ! Des Parcours Acrobatiques pour petits et grands, Jeu Mission Aventure, Crypte de Crapahute, Tyrolienne, Saut � l''�lastique invers�, Toboggan g�ant... et bien plus encore.', 2, 'fort.jpg', '25.00',true);

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `categorie` (`id`, `nom`) VALUES
(1, 'Attention'),
(2, 'Activit�'),
(3, 'Restauration'),
(4, 'H�bergement');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE IF NOT EXISTS `coffret` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statut` text NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `email` text NOT NULL,
  `msg` text,
  `type` text NOT NULL,
  `mdp` text,
  `token` text,
  `opened` boolean,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

CREATE TABLE IF NOT EXISTS `appartient` (
  `cof_id` int(11) NOT NULL AUTO_INCREMENT,
  `pre_id` int(11) NOT NULL,
  `qte` int(11) NOT NULL,
  PRIMARY KEY (`cof_id`,`pre_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

CREATE TABLE IF NOT EXISTS `note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `pre_id` int(11) NOT NULL,
  `note` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

CREATE TABLE IF NOT EXISTS `cagnotte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coff_id` int(11) NOT NULL,
  `token_gestion` text NOT NULL,
  `token_participation` text NOT NULL,
  `cloture` boolean NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

CREATE TABLE IF NOT EXISTS `participation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cagn_id` int(11) NOT NULL,
  `montant` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=5;

INSERT INTO `utilisateur` (`login`,`password`) VALUES 
("admin", '$2y$10$VmtlwF30jrlT0K7QFAd0qOKfWBIzBFjkVkVh7X3Wpq4MrvYKDStRK');

INSERT INTO `note` (`pre_id`, `note`) VALUES
(1, 4),
(2, 2),
(2, 4);