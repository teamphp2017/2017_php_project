<?php

require_once 'vendor/autoload.php';
require_once 'src/giftbox/model/Categorie.php';
require_once 'src/giftbox/model/Prestation.php';

use \giftbox\model\Categorie;
use \giftbox\model\Prestation;

echo '<body style="background-color: white">';

use Illuminate\Database\Capsule\Manager as DB;

$config=parse_ini_file('src/conf/conf.ini');

$db = new DB();
$db->addConnection( [
 'driver' => 'mysql',
 'host' => 'localhost',
 'database' => $config['dsn'],
 'username' => $config['user'],
 'password' => $config['password'],
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$listCatg = Categorie::get();

echo '<h2>Catégories :</h2>
		<ul>';

foreach($listCatg as $value){
	echo '<li>'.$value['nom'].'</br>';
	$prestations=$value->prestations;
	echo 'Prestations :<ul>';
	foreach($prestations as $value2){
		echo '<li>'.$value2['nom'].'</li>';
	}
	echo '</ul></li>';
}
 echo '</ul></br>---</br>';
 
echo '<h2>Prestations :</h2><ul>';

$listPresta= Prestation::get();

foreach($listPresta as $value){
	echo '<li style="border-width: 1px; border-style: solid; list-style-type: none; padding: 5px; background-color: white;"><h4 style="text-align: center;">'.$value['nom'].'</h4></br>'.$value['descr'].'</br><div style="text-align: center;"><img style="width: 100%; max-width: 700px;" src="img/'.$value['img'].'" /></div></br>Prix: '.$value['prix'].'</br>';
	$catg=$value->categorie;
	echo 'Catégorie : '.$catg['nom'].'</li></br>';
}
 echo '</br>---</br>';
 echo '<h2>Prestation dont l\'ID est : '.$_GET['id'].'</h2>';
$Presta= Prestation::where ('id', '=', $_GET['id'])->first();

echo '<li>'.$Presta['nom'].'</br>'.$Presta['descr'].'</br><img style="width:100px; height:100px;" src="img/'.$Presta['img'].'" /></br>Prix: '.$Presta['prix'].'</li>';;

 echo '</br>---</br>';
 
echo "</body>";